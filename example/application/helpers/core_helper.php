<?php
/**
 * 获取数据库句柄
 * @param string $server_id
 * @return resource
 */
function get_db($server_id = 'default')
{
    static $dbarray = array();
    if(count($dbarray) == 0) require_once(BASEPATH.'database/DB.php');
    
    if (!isset($dbarray[$server_id]) || empty($dbarray[$server_id])) {
        $ci = &get_instance();
		
        $ci->load->config('database');
        $dbarray[$server_id] = DB($db[$active_group], $active_record);
    }

    return $dbarray[$server_id];
}

function timeformat($the_time){
    $now_time = TIMESTAMP;
    $show_time = $the_time;
    $dur = $now_time - $show_time;
    if($dur < 60){
        return $dur.'秒前';
    }else{
        if($dur < 3600){
            return floor($dur/60).'分钟前';
        }else{
            if($dur < 86400){
                return floor($dur/3600).'小时前';
            }else{
                if($dur < 259200){//3天内
                    return floor($dur/86400).'天前';
                }else{
                    return date('Y-m-d H:i', $the_time);
                }
            }
        }
    }
}

function get_client_ip()
{
    if (!empty($_SERVER['HTTP_CLIENT_IP']))
    {
        $ip=$_SERVER['HTTP_CLIENT_IP'];
    }
    elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
    //这里用来判断是否使用代理服务器
    {
        $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
    }
    else
    {
        $ip=$_SERVER['REMOTE_ADDR'];
    }
    return $ip;
}

function delete_dir($str){
    if(is_file($str)){
        return @unlink($str);
    }elseif(is_dir($str)){
        $scan = glob(rtrim($str,'/').'/*');
        foreach($scan as $index=>$path){
            delete_dir($path);
        }
        return @rmdir($str);
    }
}