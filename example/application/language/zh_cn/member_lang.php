<?php
/**
 * 会员语言包
 *
 * @version $Id: member_lang.php
 */
return array(
		HTTP_FAILED		=>	'网络请求失败',
		LOGIN_ID_ERROR		=>	"账号信息不存在",
		LOGIN_PWD_ERROR		=>	"密码错误",
		REG_IP_ILLEGAL		=>	"IP格式错误",
		'upload_userfile_not_set'=>"无法找到名为userfile的变量",
		'upload_file_exceeds_limit'=>"上传的文件大小超过限制",
		'upload_file_exceeds_form_limit'=>"上传的文件超过尺寸限制",
		'upload_file_partial'	=>	"该文件只有部分被上传",
		'upload_no_temp_directory'=>"临时文件夹丢失",
		'upload_unable_to_write_file'=>"该文件可能不会被写入磁盘中",
		'upload_stopped_by_extension'=>"文件上传停止扩展",
		'upload_no_file_selected'=>"请选择要上传的文件",
		'upload_invalid_filetype'=>"文件类型不符合要求",
		'upload_invalid_filesize'=>'上传的文件过大',
		'upload_invalid_dimensions'=>'您上传的文件过长或者过宽',
		'upload_destination_error'=>'上传文件到指定文件夹中出现了错误',
		'upload_no_filepath'	=>'上传路径不正确',
		'upload_no_file_types'	=>'请指定允许上传的文件类型',
		'upload_bad_filename'	=>'您提交的文件名已经存在',
		'upload_not_writable'	=>'指定上传的文件夹不是可写的',
		'db_invalid_connection_str'=>'无法确定根据您所提交的数据库设置',
		'db_unable_to_connect'	=>'根据您的配置，无法连接到数据库服务器',
		'db_unable_to_select'	=>'无法选择指定的数据库: %s',
		'db_unable_to_create'	=>'无法创建指定的数据库: %s',
		'db_invalid_query'		=>'您提交的查询是无效的',
		'db_must_set_table'		=>'您必须设置查询中使用的数据表',
		'db_must_use_set'		=>'您必须使用“set”来更新一条记录',
		'db_must_use_index'		=>'您必须指定一个索引来匹配批量更新',
		'db_batch_missing_index'=>'提交批次更新的一个或多个行缺少指定索引',
		'db_must_use_where'		=>'不允许更新，除非它们包含一个“where”子句',
		'db_del_must_use_where'	=>'不允许删除，除非它们包含一个“where”或者“like”子句',
		'db_field_param_missing'=>'获取字段需要的表名作为参数',
		'db_unsupported_function'=>'您使用的数据库不允许使用这个功能',
		'db_transaction_failure'=>'交易失败：执行回滚',
		'db_unable_to_drop'		=>'无法删除指定的数据库',
		'db_unsuported_feature'	=>'您使用的数据库平台不支持此功能',
		'db_unsuported_compression'=>'您选择的文件压缩格式不支持你的服务器',
		'db_filepath_error'		=>'无法写入数据到您所提交的文件路径',
		'db_invalid_cache_path'	=>'您所提交的缓存路径是无效或不可写',
		'db_error_heading'		=>'出现数据库错误',
		'db_unable_to_set_charset'=>'无法设置客户端连接的字符集: %s',
		'db_column_definition_required'=>'该操作需要定义一个字段',
		'db_column_name_required'=>'该操作需要指定一个字段名',
		'db_table_name_required'	=>'该操作需要指定一个表名',
		'db_invalid_cache_path'	=>'您所提交的缓存路径是无效或不可写'
);
/* End of file member_lang.php */
/* Location: ./system/language/zh_cn/member_lang.php */