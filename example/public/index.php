<?php
switch (ENVIRONMENT)
{
	case DEVELOPMENT:
	case TEST:
		error_reporting(E_ERROR);
		break;
	case PRODUCTION:
	default:
		error_reporting(0);
		break;
}

$application_folder = 'application';
$dir_path = './';
if (strpos(__FILE__, '\\') !== false) {
	$dir_path =  substr(__FILE__, 0, strrpos(__FILE__, '\\')) . '/';
} elseif (strpos(__FILE__, '/') !== false) {
	$dir_path =  substr(__FILE__, 0, strrpos(__FILE__, '/')) . '/';
}
define('BASEPATH', str_replace("\\", "/", realpath($dir_path.'../').'/'));
define('APPPATH', realpath(BASEPATH.$application_folder).'/');

$framework = new wk_framework();
$framework->loadView();
$framework->setTemplateDir(APPPATH.'../public/template/');
$framework->setCompileDir(APPPATH.'../public/data/complie/');
$framework->setApplicationPath(APPPATH);
$framework->setEnvironment(ENVIRONMENT);
require_once(APPPATH.'core/WK_Controller.php');
$framework->initialize();
$framework->captureRouter();