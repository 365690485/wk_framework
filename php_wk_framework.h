/*
  +----------------------------------------------------------------------+
  | PHP Version 5                                                        |
  +----------------------------------------------------------------------+
  | Copyright (c) 1997-2013 The PHP Group                                |
  +----------------------------------------------------------------------+
  | This source file is subject to version 3.01 of the PHP license,      |
  | that is bundled with this package in the file LICENSE, and is        |
  | available through the world-wide-web at the following url:           |
  | http://www.php.net/license/3_01.txt                                  |
  | If you did not receive a copy of the PHP license and are unable to   |
  | obtain it through the world-wide-web, please send a note to          |
  | license@php.net so we can mail you a copy immediately.               |
  +----------------------------------------------------------------------+
  | Author:                                                              |
  +----------------------------------------------------------------------+
*/

/* $Id$ */

#ifndef PHP_WK_FRAMEWORK_H
#define PHP_WK_FRAMEWORK_H

extern zend_module_entry wk_framework_module_entry;
#define phpext_wk_framework_ptr &wk_framework_module_entry

#define PHP_WK_FRAMEWORK_VERSION "0.1.0" /* Replace with version number for your extension */

#ifdef PHP_WIN32
#	define PHP_WK_FRAMEWORK_API __declspec(dllexport)
#elif defined(__GNUC__) && __GNUC__ >= 4
#	define PHP_WK_FRAMEWORK_API __attribute__ ((visibility("default")))
#else
#	define PHP_WK_FRAMEWORK_API
#endif
 
#ifdef ZTS
#include "TSRM.h"
#endif

#define FETCH_THIS                    Z_OBJCE_P(getThis()), getThis()
#define EXT_STARTUP_FUNCTION(module)  ZEND_MINIT_FUNCTION(module)
#define EXT_STARTUP(module)           ZEND_MODULE_STARTUP_N(module)(INIT_FUNC_ARGS_PASSTHRU)

#define VAR_DUMP(instance) php_var_dump(&instance, 1 TSRMLS_CC);
#define VAR_DUMP_PP(instance) php_var_dump(instance, 1 TSRMLS_CC);

#define VAR_TYPE				unsigned int
#define POST 					TRACK_VARS_POST
#define GET     				TRACK_VARS_GET
#define ENV     				TRACK_VARS_ENV
#define FILES   				TRACK_VARS_FILES
#define SERVER  				TRACK_VARS_SERVER
#define REQUEST 				TRACK_VARS_REQUEST
#define COOKIE  				TRACK_VARS_COOKIE

#define get_array_item_p(arr, name, val, default, len) if( zend_hash_find(arr, name, strlen(name)+1, (void**)&val) != SUCCESS){\
			MAKE_STD_ZVAL(*val);\
			ZVAL_STRINGL(*val, default, len, 0);\
		}

PHP_MINIT_FUNCTION(wk_framework);
PHP_MSHUTDOWN_FUNCTION(wk_framework);
PHP_RINIT_FUNCTION(wk_framework);
PHP_RSHUTDOWN_FUNCTION(wk_framework);
PHP_MINFO_FUNCTION(wk_framework);

PHP_FUNCTION(wk_get_instance);	/* For testing, remove later. */
PHP_FUNCTION(new_array_to_xml);

/* 
	Declare any global variables you may need between the BEGIN
	and END macros here:     

ZEND_BEGIN_MODULE_GLOBALS(wk_framework)
	long  global_value;
	char *global_string;
ZEND_END_MODULE_GLOBALS(wk_framework)
*/

/* In every utility function you add that needs to use variables 
   in php_wk_framework_globals, call TSRMLS_FETCH(); after declaring other 
   variables used by that function, or better yet, pass in TSRMLS_CC
   after the last function argument and declare your utility function
   with TSRMLS_DC after the last declared argument.  Always refer to
   the globals in your function as WK_FRAMEWORK_G(variable).  You are 
   encouraged to rename these macros something shorter, see
   examples in any other php module directory.
*/

extern int include(char *path);
extern zval * request_var(uint type, char * name, uint len TSRMLS_DC);
extern zend_class_entry *get_class_entry(char *class_name, int len TSRMLS_DC);
extern zend_class_entry *wk_framework_ce;

typedef void(*include_callback)(HashTable *active_symbol_table);
char *get_config_path(char *filename, int len);

#ifdef ZTS
#define WK_FRAMEWORK_G(v) TSRMG(wk_framework_globals_id, zend_wk_framework_globals *, v)
#else
#define WK_FRAMEWORK_G(v) (wk_framework_globals.v)
#endif

#endif	/* PHP_WK_FRAMEWORK_H */


/*
 * Local variables:
 * tab-width: 4
 * c-basic-offset: 4
 * End:
 * vim600: noet sw=4 ts=4 fdm=marker
 * vim<600: noet sw=4 ts=4
 */